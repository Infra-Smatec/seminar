# タイトル
Linuxのコマンド

## 目次
- lsコマンド
  - `-l`オプション
- pwdコマンド
- cpコマンド
  - `-p`オプション

### lsコマンド
カレントディレクトリのファイルを表示します。
```
$ ls
testdir  testfile  testfile1  testfile3

$ ls -l
testdir  testfile  testfile1  testfile3
```

### pwdコマンド
カレントディレクトリを表示します。
```
$ pwd
/home/makoto/testdir
```


### cpコマンド
ファイルをコピーします。
```
$ cp testfile1 testfile100
```

## 参考文献
[
Linuxコマンド一覧](http://taku.adachi-navi.com/sankou/RedHat/command_linux/~mms/unix/linux_com/index.html)

## 書いた人
[スマートテッククロス：黒木真](https://gitlab.com/makoto517)  